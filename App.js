import React from 'react';
import { NativeRouter, Route, Link, Switch } from "react-router-native";

import { About } from "./Components/About";
import Home from "./Components/Home";

const App = () => {
    return (
      <NativeRouter>
        <Switch>
          <Route exact path="/" component={ Home }/>
          <Route path="/about" component={ About }/>
        </Switch>
      </NativeRouter>
    );
};
export default App;

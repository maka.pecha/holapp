import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
} from "react-native";
import {ButtonStyles} from "./ButtonStyles";

const Button = (props) => {
  const {name, onPress} = props;
  return (
    <TouchableOpacity style={ButtonStyles.button} onPress={onPress}>
      <Text>{name}</Text>
    </TouchableOpacity>
  );
};

export {Button};
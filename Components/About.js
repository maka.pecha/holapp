import React, {Component} from 'react';
import {
  StyleSheet,
  Text
} from "react-native";
import { Container, Header, Content, Footer} from "native-base";
import {Button} from "./Button";

const About = ({ location, history }) => {
  return (
    <Container style={styles.container}>
      <Header style={styles.header}>
        <Text>HolApp</Text>
      </Header>
      <Content contentContainerStyle={styles.content}>
        <Text style={styles.contentText}> Hola {location.state.name}!</Text>
        <Button
          name="Go Home"
          onPress={() => {
            history.push({
              pathname: "/",
            });
          }}
        />
      </Content>
      <Footer style={styles.footer}>
        <Text>Curso React Native</Text>
      </Footer>
    </Container>
  )
};

export {About};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
  },
  content: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "skyblue",
    alignItems: "center",
    justifyContent: "center",
  },
  contentText: {
    color: "white",
  },
  header: {
    backgroundColor: "powderblue",
    alignItems: "center",
  },
  footer: {
    flexDirection: "row",
    backgroundColor: "steelblue",
    alignItems: "center",
  },
});
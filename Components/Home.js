import React, { Component } from "react";
import { StyleSheet, Text, TextInput } from "react-native";
import { Container, Content, View, Header, Footer, Button } from "native-base";
import isDisabled from "react-native-web/dist/modules/AccessibilityUtil/isDisabled";
// import {Button} from "./Button";

export default class Home extends Component {
  constructor(props) {
    super();
    this.state = {
      name: "",
    };
  }

  render() {
    const { history } = this.props;
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Text>HolApp</Text>
        </Header>
        <Content contentContainerStyle={styles.content}>
          <Text style={styles.contentText}>Enter your Name</Text>
          <TextInput
            style={{
              height: 40,
              width: 200,
              borderColor: "#AAAAAA",
              borderWidth: 2,
              marginTop: 30,
              padding: 10
            }}
            onChangeText={(text) =>
              this.setState({ name: text })
            }
            value={this.state.name}
          />
          <Button
            style={ !this.state.name ? styles.buttonDisabled : styles.button}
            disabled={!this.state.name}
            onPress={() => {
              history.push({
                pathname: "/about",
                state: { name: this.state.name }
              });
            }}
          >
            <Text>Say Hello</Text>
          </Button>
        </Content>
        <Footer style={styles.footer}>
          <Text>Curso React Native</Text>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
  },
  content: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "skyblue",
    alignItems: "center",
    justifyContent: "center",
  },
  contentText: {
    color: "white",
  },
  header: {
    backgroundColor: "powderblue",
    alignItems: "center",
  },
  footer: {
    flexDirection: "row",
    backgroundColor: "steelblue",
    alignItems: "center",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 15,
    borderRadius: 10,
    margin: 20,
    marginTop: 30,
  },
  buttonDisabled: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 15,
    borderRadius: 10,
    margin: 20,
    marginTop: 30,
    opacity: 0.7
  }
});
import {StyleSheet} from 'react-native';

const ButtonStyles = StyleSheet.create({
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 15,
    borderRadius: 10,
    margin: 20,
    marginTop: 50,
  }
});

export {ButtonStyles};